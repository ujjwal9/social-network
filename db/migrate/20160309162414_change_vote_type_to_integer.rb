class ChangeVoteTypeToInteger < ActiveRecord::Migration
  def change
  	remove_column :votes, :vote
  	add_column :votes, :vote, :integer, default: 0
  end
end
