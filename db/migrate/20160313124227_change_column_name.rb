class ChangeColumnName < ActiveRecord::Migration
  def change
  	rename_column :votes, :vote, :emotion
  end
end
