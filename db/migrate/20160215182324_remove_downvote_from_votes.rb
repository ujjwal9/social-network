class RemoveDownvoteFromVotes < ActiveRecord::Migration
  def change
  	remove_column :votes, :downvote
  	rename_column :votes, :upvote, :vote 
  	change_column_default :votes, :vote, nil
  end
end
