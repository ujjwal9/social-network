class CreateVotes < ActiveRecord::Migration
  def change
    create_table :votes do |t|
      t.boolean :upvote
      t.boolean :downvote
      t.references :user, foreign_key: true
      t.belongs_to :votable, polymorphic: true

      t.timestamps null: false
    end
    add_index :votes, [ :votable_id, :votable_type, :user_id]
  end
end
