class CreateComments < ActiveRecord::Migration
  def change
    create_table :comments do |t|
      t.string :commentable_type
      t.integer :commentable_id
      t.integer :user_id
      t.text :body

      t.timestamps null: false
    end
    add_index :comments, [:commentable_id, :commentable_type]
  end
end
