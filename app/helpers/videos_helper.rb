module VideosHelper

	def embed(youtube_url)
		youtube_id = youtube_url.split("=").last
		content_tag(:iframe, nil,autohide: 0, :allowfullscreen => "allowfullscreen", src: "//www.youtube.com/embed/#{youtube_id}")
	end
	
end
