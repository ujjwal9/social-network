json.array!(@videos) do |video|
  json.extract! video, :id, :url, :user_id, :title, :description
  json.url video_url(video, format: :json)
end
