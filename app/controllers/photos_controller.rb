class PhotosController < ApplicationController
	before_action :set_photo, only: [:show, :edit, :delete, :destroy]
	before_action :authenticate_user!, except: [:index,:show]

	def index 
		@photos = Photo.order('created_at')
	end

	def new 
		@photo = current_user.photos.build
	end

	def create
		@photo = current_user.photos.build(photo_params)
		if @photo.save
			flash[:success] = 'The photo was added!'
			redirect_to :photos
		else
			render 'new'
		end
	end

	def destroy
		@photo.destroy
		respond_to do |format|
			format.html {redirect_to photos_url, notice: 'Photo successfully destroyed.'}
			format.json { head :no_content }
		end
	end

	def edit	
	end

	def show
		@photo = Photo.find(params[:id])
		@commentable=@photo
	    @comments = @commentable.comments
	    @comment = Comment.new

	    @votable=@photo
	    # @likes = @votable.votes.where(:emotion => 1).count
	    # @dislikes = @votable.votes.where(:emotion => 2).count
	end

	private

	def photo_params
		params.require(:photo).permit(:image, :title,:user_id,:photo_id)
	end

	def set_photo
		@photo = Photo.find(params[:id])
	end
end
