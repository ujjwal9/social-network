class CommentsController < ApplicationController
	before_action :set_comment, only: [:show, :edit, :delete, :destroy]
	before_action :authenticate_user!, except: [:index, :show]
	before_action :load_commentable
	before_action :set_parent_comment

	def index
		@comments = @commentable.CommentsController
	end

	#reply new action
	def new
		@comment = @commentable.comments.new
		respond_to do |format|
			format.js
		end
	end

	# for reply
	def show
		respond_to do |format|
			format.js
		end
	end

	def create 
		@comment = @commentable.comments.build(comment_params)
		@comment.user_id = current_user.id
		if params[:comment_id]
			@comment.parent_id=params[:comment_id]
		end

		if @comment.save
			respond_to do |format|
				format.html{ redirect_to @commentable, notice: "Comment created" }
				format.js # render comments/create.js.erb
			end
		else
			render :new
		end
	end

	def destroy
		@comment.destroy
		respond_to do |format|
			format.html{redirect_to @commentable, notice: 'Comment was successfully destroyed'}
			format.json { head :no_content}
		end
	end

	private

		def comment_params
			params.require(:comment).permit(:body,:user_id,:article_id,:parent_id)
		end

		# def load_commentable
		# 	@commentable=@able
		# end
		#gives the commentable object
		def load_commentable
			klass = [Article,Photo,Link,Video].detect { |c| params["#{c.name.underscore}_id"]}
			@commentable = klass.find(params["#{klass.name.underscore}_id"])
		end

		def set_comment
			@comment = Comment.find(params[:id])
		end

		def set_parent_comment
			unless params[:comment_id].nil?
			 	@parent_comment = Comment.find(params[:comment_id])
			end 
		end

end
