class  VotesController < ApplicationController
	before_action :authenticate_user!
	before_filter :load_votable, :voteExists , only: [:upvote, :downvote]

	def upvote
		if @vote.blank?#returns an empty array not nil
			@vote = @votable.votes.new(:emotion => 1)
			@vote.user_id = current_user.id
			if @vote.save
				respond_to do |format|
					format.js
					format.html{redirect_to @votable }
				end
			end
		else
			if @vote[0].update(:emotion => 1)
				respond_to do |format|
					format.js
					format.html{redirect_to @votable }
				end
			end
		end	
	end

	def downvote
		if @vote.blank?
			@vote = @votable.votes.new(:emotion => 2)
			@vote.user_id = current_user.id
			if @vote.save
				respond_to do |format|
					format.js
					format.html{redirect_to @votable }
				end
			end
		else
			if @vote[0].update(:emotion => 2)
				respond_to do |format|
					format.js
					format.html{redirect_to @votable }
				end
			end	
		end
	end


	private

	def load_votable		
		klass = [Comment,Article,Photo,Link,Video].detect { |c| params["#{c.name.underscore}_id"]}
		@votable = klass.find(params["#{klass.name.underscore}_id"])
	end

	def voteExists
		#returns an array
		@vote = Vote.where(:votable => @votable, :user_id => current_user.id)
	end

end