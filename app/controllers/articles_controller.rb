class ArticlesController < ApplicationController
	before_action :set_article, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!, except: [:index, :show]

  def index
  	@articles= Article.all
  	#render json: @article 
  end

  def new 
  	@article= current_user.articles.build
  end

  def edit
  end

  def create
  	@article = current_user.articles.build(article_params)

  	respond_to do |format|
  		if @article.save
  			format.html {redirect_to @article,notice: 'Article was successfuly created'}
  			format.json {render :show, status: :created , location: @artilce }
  		else
  			format.html {render :new}
  			format.json {render json: @article.errors, status: :unprocessable_entity}
  		end
  	end
  end

  def show
    @commentable=@article
    @comments = @commentable.comments
    @comment = Comment.new

     @votable=@article
    # @likes = @votable.votes.where(:emotion => 1).count
    # @dislikes = @votable.votes.where(:emotion => 2).count
  end

  def destroy
  	@article.destroy
  	respond_to do |format|
  		format.html {redirect_to articles_url, notice: 'Article was successfuly destroyed'}
  		format.json { head :no_content}
  	end
  end

  def update
  	respond_to do |format|
  		if @article.update(article_params)
  			format.html {redirect_to @article, notice: 'Article was successfuly updated'}
  			format.json {render :show, status: :updated , location: @article}
  		else
  			format.html {render :edit}
  			format.json {render json: @article.errors, status: :unprocessable_entity}
  		end
  	end
  end

  private

    def set_article
    	@article = Article.find(params[:id])
    end

    def article_params
    	params.require(:article).permit(:title, :text)
    end
end
