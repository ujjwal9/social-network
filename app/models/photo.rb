class Photo < ActiveRecord::Base
	belongs_to :user
	has_many :comments, :as => :commentable
	has_many :votes, :as => :votable
	
	has_attached_file :image

	validates_attachment :image, content_type: {content_type: ["image/jpeg", "image/gif", "image/png"]}
end
