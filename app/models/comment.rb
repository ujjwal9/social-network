class Comment < ActiveRecord::Base
	belongs_to :commentable, polymorphic: true
	belongs_to :user
	has_many :replies
	has_many :votes, :as => :votable, :dependent => :destroy
end
